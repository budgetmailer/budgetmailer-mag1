# BudgetMailer Magento 1 API Client

This repository contains official BudgetMailer client for Magento 1.

## Requirements

- PHP >= 5.3
- [PHP module HASH](http://php.net/manual/en/book.hash.php)
- [PHP module JSON](http://php.net/manual/en/book.json.php)
- [PHP module Sockets](http://php.net/manual/en/book.sockets.php)
- [BudgetMailer API Account](https://www.budgetmailer.nl/aanmelden.php)
- Magento version 1.6 or higher

## Installation

You can install module using one of the following methods.

### Magento Connect

- [Professio BudgetMailer Integration](https://www.magentocommerce.com/magento-connect/professio-budgetmailer-integration-1.html)

### Modman

In root of your Magento installation initiate modman if you didn't do it already:

- `modman init`

Then clone the extension from repository:

- `modman clone git@gitlab.com:budgetmailer/budgetmailer-mag1.git`

## Configuration

After module installation go to Magento Configuration back-end, then in menu
select `System` -> `Configuration`, and click on `BudgetMailer` in `Customers` tab.

The required configuration directives are: `API Endpoint`, `API Key`, `API Secret` and `List`.

After initial API configuration is saved, the list of mailing lists will 
be loaded automatically.

## Copyright

MIT License

## Contact Information

- Email: [info@budgetmailer.nl](mailto:info@budgetmailer.nl)
- Website: [BudgetMailer](https://www.budgetmailer.nl/index.php)

## Changelog

- 1.0.4 (2016-12-20):
    - Changed copyright years
    - Removed contact and list models and all data in database to simpler the maintenance and workflow
    - Replaced custom BudgetMailer Magento API client with BudgetMailer PHP API client
    - Translation / Templates updates

- 1.0.3:
    - Added configuration for account registration subscribe checkbox
    - Added subscribe option for guests
    - Added export unregistered customers
    - Minor data mapping fixes
    - Removed custom Magento customers and newsletter subscribers mass actions
    - Templates / Translation updates

- 1.0.2:
    - Fixed: `can't use method return value in write context` in Contact model

- 1.0.1:
    - Added customer tagging by category names
    - Added translation
    - Minor fixes in debug logging

- 1.0.0:
    - Initial version
